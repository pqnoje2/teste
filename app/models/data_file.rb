class DataFile < ActiveRecord::Base
	def self.save(upload)
		name = upload['datafile'].original_filename
		directory = "public/upload"
		path = File.join(directory, name)
		File.open(path, "wb") {|f| f.write(upload['datafile'].read)}
	end

	def self.read(upload)
		name = upload['datafile'].original_filename
		directory = "public/upload"
		path = File.join(directory, name)
		File.read(path)
	end

	def self.parse_tab_file(file)
		parsed_data = Array.new
		file.split(/\n/).each do |line|
			length = line.length
			line_data = Array.new
			line.split(/\t/).each do |value| 				
				line_data << value
			end
			parsed_data << line_data
		end

		parsed_data
	end
end
