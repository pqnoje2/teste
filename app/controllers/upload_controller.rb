class UploadController < ApplicationController
	def index
		render :file => 'app\views\upload\upload_file.rhtml'
	end

	def upload_file
		DataFile.save(params[:upload])
		data = DataFile.read(params[:upload])
		@parsed_data_header = DataFile.parse_tab_file(data).first
		@parsed_data = DataFile.parse_tab_file(data).last(@parsed_data_header.length-2)

		@parsed_data.each do |value|		

			address = Address.new
			address.principal = value[4]
			address.save

			provider = Provider.new
			provider.name = value[5]
			provider.address = address
			provider.save

			product = Product.new
			product.description = value[1]
			product.price = value[2]
			product.provider = provider
			product.save

			customer = Customer.new
			customer.name = value[0]
			customer.save

			sale = Sale.new
			sale.quantity = value[3]
			sale.customer = customer
			sale.product = product
			sale.save
		end

		@sales = Sale.all

		render :file => 'app\views\upload\uploaded_file.rhtml'
	end
end
