json.array!(@addresses) do |address|
  json.extract! address, :id, :principal
  json.url address_url(address, format: :json)
end
