json.array!(@sales) do |sale|
  json.extract! sale, :id, :quantity, :product_id, :customer_id
  json.url sale_url(sale, format: :json)
end
