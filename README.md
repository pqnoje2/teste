#Instruções

##Instalação

instalar o GIT: https://git-scm.com/downloads
instalar o Ruby: https://www.ruby-lang.org/en/documentation/installation/
instalar o Rails

```

$ gem install rails
```

Clonar o teste do repositório
```

$ git clone https://pqnoje2@bitbucket.org/pqnoje2/teste.git
```

Migrar o banco de dados
```

$ rake db:migrate
```

Iniciar o servidor
```
$ rails server
```

Finalmente, acessar o site
abrir o navegador e digitar: http://localhost:3000/